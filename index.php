<?php

require_once 'vendor/autoload.php';

use App\XmlAmazonParser;
use App\Request;
use App\Exception\BadHttpCodeException;
use App\File;

$request = new Request;

try {
    $content = $request->get('https://webtest.d0.acom.cloud/test/xml-examples/example-footwearnews.xml');
} catch (BadHttpCodeException $e) {
    die($e->getMessage());
}

$xmlParser = new XmlAmazonParser($content);
$results = $xmlParser->getResults();

File::saveAsCsv($results, 'domain_date' . date('Y-m-d') . '.csv');
echo 'File saved successfully';

