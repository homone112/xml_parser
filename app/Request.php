<?php

namespace App;

use App\Exception\BadHttpCodeException;

class Request
{
     // Success HTTP CODE
    const HTTP_OK = 200;

    /**
     * HTTP status code from server
     */
    private int $statusCode;

    /**
     * Send Request
     *
     * @param string $url 
     * @return void
     */
    private function sendRequest(string $url)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $response = curl_exec($ch); 
        $this->statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $response;
        
    }

    /**
     * Send http get request
     *
     * @param string $url
     * @return void
     */
    public function get(string $url)
    {
        $response = $this->sendRequest($url);
        
        if ($this->statusCode !== self::HTTP_OK) {
            throw new BadHttpCodeException(self::HTTP_OK, $this->statusCode);
        }
        
        return $response;
    }
}
