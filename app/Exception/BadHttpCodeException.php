<?php

namespace App\Exception;

class BadHttpCodeException extends \Exception
{
    public function __construct($expectingHttpCode, $receivedHttpCode)
    {
        $message = "Expecting http response code $expectingHttpCode but received $receivedHttpCode";
        parent::__construct($message);
    }
}
