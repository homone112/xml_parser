<?php

namespace App;

class XmlAmazonParser extends XmlParser
{

    /**
     * Xml content
     */
    protected string $content;

    /**
     * @param string $content
     */
    public function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * Parse Content
     *
     * @return array
     */
    public function getResults(): array
    {
        $this->load();

        $results = [];
        foreach ($this->xml->channel->item as $item) {
            $introText = strip_tags($item->xpath('content:encoded')[0]);
            $link = $item->link;
            $item = $item->xpath('amzn:products');
            foreach ($item as $products) {
                $products = $products->xpath('amzn:product');
                foreach ($products as $product) {
                    $award = $product->xpath('amzn:award')[0];
                    $productSummary = $product->xpath('amzn:productSummary')[0];
                    $url = $product->xpath('amzn:productURL')[0];
                    $uriParams = explode("/", rtrim($url, '/'));
                    $results[] = [
                        'ASIN' => end($uriParams),
                        'URL' => $link,
                        'Amazon Url' => $url,
                        'Product Name' => $product->xpath('amzn:productHeadline')[0],
                        'Amazon Introtext' => $introText,
                        'Amazon Introtext COUNT' => strlen($introText),
                        'Amazon Product Summary' => $productSummary,
                        'Amazon Product Summary COUNT' => strlen($productSummary),
                        'Amazon Award' => $award,
                        'Amazon Award COUNT' => strlen($award)
                    ];
                }
            }
        }
        
        return $results;
    }
}
