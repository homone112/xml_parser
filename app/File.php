<?php

namespace App;

class File
{

    /**
     * Save associative array as csv file
     *
     * @param array $rows
     * @param string $path
     * @return void
     */
    public static function saveAsCsv(array $rows, string $path)
    {
        $file = fopen($path, "w");
        $hasHeader = false;
        foreach ($rows as $row) {
            if (!$hasHeader) {
                fputcsv($file, array_keys($row));
                $hasHeader = true;
            }
            fputcsv($file, $row);
        }

        fclose($file);
    }
}
