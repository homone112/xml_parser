<?php

namespace App;

/**
 * Class wrapper for simplexml
 */
class XmlParser
{

    /**
     * xml parser
     */
    protected object $xml;

    /**
     * xml content
     */
    protected string $content;

    /**
     *
     * @param string $content
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Load xml content
     *
     * @return void
     */
    protected function load()
    {
        $this->xml = simplexml_load_string($this->content, 'SimpleXMLElement', LIBXML_NOCDATA);
    }

}
